/**
 * Required External Modules
 */

import * as dotenv from 'dotenv'
import express from 'express'
import cors from 'cors'
import helmet from 'helmet'
import mongoose from 'mongoose'

import { vaultEntriesRouter } from './vault-entries/vault-entries.router'
import { errorHandler } from './middleware/error.middleware'
import { notFoundHandler } from './middleware/not-found.middleware'

dotenv.config()

/**
 * App Variables
 */

if (!process.env.PORT) {
  process.exit(1)
}

const PORT: number = parseInt(process.env.PORT as string, 10)
const app = express()

/**
 *  App Configuration
 */

app.use(helmet())
app.use(cors())
app.use(express.json())
app.use('/api/vaultEntries', vaultEntriesRouter)

app.use(errorHandler)
app.use(notFoundHandler)

/**
 * Database Configuration
 */
const dbUrl = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@cluster0.spmfn.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`
mongoose.connect(dbUrl, { useNewUrlParser: true })

const db = mongoose.connection
db.once('open', (_) => {
  console.log('Database connected')
})

db.on('error', (err) => {
  console.error('connection error: ', err)
})

/**
 * Server Activation
 */

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`)
})
