import bcrypt from "bcrypt"

export async function hashString(plaintext: string): Promise<string> {
  const saltRounds = 10

  return await bcrypt.hash(plaintext, saltRounds)
}
