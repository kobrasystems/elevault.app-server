export const jwtDecode = (token: string) => {
  const base64 = token.split('.')[1]
  const decoded = JSON.parse(Buffer.from(base64, 'base64').toString('utf8'))

  return decoded
}
