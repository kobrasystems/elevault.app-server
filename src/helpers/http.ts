import { Request } from 'express'

export const getAuthorizationFromHeaders = (request: Request) => {
  const authHeader = request.headers.authorization

  if (authHeader === undefined) {
    throw Error('Could not get authorization header')
  }

  return authHeader.substr(7)
}
