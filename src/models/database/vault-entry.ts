import mongoose from 'mongoose'

const Schema = mongoose.Schema

const VaultEntrySchema = new Schema({
  userId: { type: String, required: true },
  dayRating: Number,
  mood: { type: String, required: true },
  events: [String],
  comments: [String],
  date: { type: Date, required: true },
})

export default mongoose.model('VaultEntry', VaultEntrySchema)
