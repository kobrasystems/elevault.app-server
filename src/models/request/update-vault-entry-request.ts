export default interface UpdateVaultEntryRequest {
  _id: string
  dayRating: number
  mood: string
  events: string[]
  comments: string[]
  date: Date
}
