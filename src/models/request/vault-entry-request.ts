export default interface VaultEntry {
  dayRating: number
  mood: string
  events: string[]
  comments: string[]
  date: Date
}
