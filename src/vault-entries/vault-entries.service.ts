import VaultEntry from '../models/database/vault-entry'
import VaultEntryRequest from '../models/request/vault-entry-request'
import UpdateVaultEntryRequest from '../models/request/update-vault-entry-request'

export async function getVaultEntries(
  userId: string,
  startDate: Date,
  endDate: Date
) {
  return await VaultEntry.find({
    userId: userId,
    date: { $gte: startDate, $lte: endDate },
  })
    .sort({ date: 1 })
    .exec()
}

export async function createVaultEntry(
  userId: string,
  newVaultEntry: VaultEntryRequest
) {
  const vaultEntry = new VaultEntry({ ...newVaultEntry, userId })
  const document = await vaultEntry.save()

  return document
}

export async function updateVaultEntry(
  userId: string,
  _id: string,
  existingVaultEntry: UpdateVaultEntryRequest
) {
  const vaultEntry = await VaultEntry.findOne({ userId: userId, _id: _id })

  vaultEntry.mood = existingVaultEntry.mood
  vaultEntry.events = existingVaultEntry.events
  vaultEntry.dayRating = existingVaultEntry.dayRating
  vaultEntry.comments = existingVaultEntry.comments

  const savedVaultEntry = await vaultEntry.save()

  return savedVaultEntry
}
