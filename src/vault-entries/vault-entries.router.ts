/**
 * Required External Modules and Interfaces
 */

import express, { Request, Response } from 'express'

import * as VaultEntryService from './vault-entries.service'
import { checkJwt } from '../middleware/authz.middleware'
import { getAuthorizationFromHeaders } from '../helpers/http'
import { jwtDecode } from '../helpers/jwt'

/**
 * Router Definition
 */

export const vaultEntriesRouter = express.Router()

vaultEntriesRouter.use(checkJwt)

/**
 * Controller Definitions
 */

// GET vault entries
vaultEntriesRouter.get('/', async (request: Request, response: Response) => {
  try {
    const encodedToken = getAuthorizationFromHeaders(request)
    const decodedToken = jwtDecode(encodedToken)

    const startDateQueryParam = request.query.startDate?.toString() || ''
    const endDateQueryParam = request.query.endDate?.toString() || ''

    if (startDateQueryParam === '' || endDateQueryParam === '') {
      response.status(400).send('startDate and endDate are required')
    } else {
      const startDate = new Date(startDateQueryParam)
      const endDate = new Date(endDateQueryParam)

      const vaultEntries = await VaultEntryService.getVaultEntries(
        decodedToken.sub,
        startDate,
        endDate
      )

      response.status(200).send(vaultEntries)
    }
  } catch (e) {
    response.status(500).send(e.message)
  }
})

// GET items/:id

// POST items
vaultEntriesRouter.post('/', async (request: Request, response: Response) => {
  try {
    const encodedToken = getAuthorizationFromHeaders(request)
    const decodedToken = jwtDecode(encodedToken)

    const vaultEntry = await VaultEntryService.createVaultEntry(
      decodedToken.sub,
      request.body
    )
    response.status(200).send(vaultEntry)
  } catch (e) {
    response.status(500).send(e.message)
  }
})

vaultEntriesRouter.put('/:id', async (request: Request, response: Response) => {
  try {
    const encodedToken = getAuthorizationFromHeaders(request)
    const decodedToken = jwtDecode(encodedToken)

    const vaultEntryId = request.params.id

    const vaultEntry = await VaultEntryService.updateVaultEntry(
      decodedToken.sub,
      vaultEntryId,
      request.body
    )

    response.status(200).send(vaultEntry)
  } catch (e) {
    response.status(500).send(e.message)
  }
})

// PUT items/:id

// DELETE items/:id
